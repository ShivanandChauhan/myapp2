//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:myapp/ui/quiz.dart';
import './result.dart';

class ITQuiz extends StatefulWidget {
  

  @override
  State<StatefulWidget> createState() {
    return new MYITQuizPage();
  }
}

class MYITQuizPage extends State<ITQuiz> {
  
  final _questionsIT = const [
    {
      'questionText': 'Who invented Compact Disc?',
      'answers':[{'text': 'James T Russel','score':1},
      {'text': 'James A Gosling','score':0},
      {'text': 'J.M. Coetzee','score':0},
      {'text': 'Tim Burners Lee','score':0}
    ],  
    },
    {
      'questionText': 'Who invented Java?',
      'answers':[{'text': 'Shakunthala Devi','score':0},
     {'text': 'James A Gosling','score':1},
      {'text': 'J.M. Coetzee','score':0},
      {'text': 'Tim Burners Lee','score':0}
    ],  
    },
    {
      'questionText': 'Longhorn was the code name of ?',
      'answers':[{'text': 'Windows server','score':0},
      {'text': 'Windows XP','score':0},
      {'text': 'Windows 10','score':0},
      {'text': 'Windows Vista','score':1}
    ],  
    },
    {
      'questionText': 'Who is known as the Human Computer of India?',
      'answers':[{'text': 'Sundar pichai','score':0},
      {'text': 'Rama Krishana','score':0},
      {'text': 'Shakunthala Devi','score':1},
      {'text': 'Anjali Panchal','score':0}
    ],  
    },
    {
      'questionText': 'What is mean by Liveware?',
      'answers':[{'text': 'People who work with the computer','score':1},
      {'text': 'People who work for the computer','score':0},
      {'text': 'People who work with the company','score':0},
      {'text': 'People who work for the company','score':0}
    ],  
    },
    {
      'questionText': 'Which computer engineer got Nobel Prize for literature in 2003?',
      'answers':[{'text': 'James T Russel','score':0},
      {'text': 'James A Gosling','score':0},
      {'text': 'J.M. Coetzee','score':1},
      {'text': 'Tim Burners Lee','score':0}
    ],  
    },
    {
      'questionText': 'Do no evil\' is tag line of ......',
      'answers':[{'text': 'Drive','score':0},
      {'text': 'Mozilla firefox','score':0},
      {'text': 'Yahoo','score':1},
      {'text': 'Google','score':0}
    ],  
    },
    {
      'questionText': 'What is the extension of PDF?',
      'answers':[{'text': 'Portable document format','score':1},
      {'text': ' Processed document format','score':0},
      {'text': ' Portable drive Fix','score':0},
      {'text': ' Portable Disk format','score':0}
    ],  
    },

  ];
  
var _questionIndex=0;
var _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }
  void _answerQuestion(int score)
  {
        _totalScore += score;

    setState(() {
      _questionIndex+=1;
    });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return  Scaffold(
      appBar:  AppBar(
        title:  Text('Quiz Page'),
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
      ),
      body:  _questionIndex < _questionsIT.length
       ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questionsIT,
              )
            : Result(_totalScore, _resetQuiz),

        
    );
  }
}
