//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:myapp/ui/quiz.dart';
import './result.dart';

class GeneralQuiz extends StatefulWidget {
  

  @override
  State<StatefulWidget> createState() {
    return new MYGeneralQuizPage();
  }
}

class MYGeneralQuizPage extends State<GeneralQuiz> {
  
  final _questions = const [
    {
      'questionText': 'What is the largest organ of the human body?',
      'answers':[{'text': 'Liver','score':0},
      {'text': 'large Intestine','score':0},
      {'text': 'Heart','score':0},
      {'text': 'Skin','score':1}
    ],  
    },
    {
      'questionText': 'In Roman Numerals, what does XL equate to?',
      'answers':[{'text': '60','score':0},
      {'text': '50','score':0},
      {'text': '40','score':1},
      {'text': '44','score':0}
    ],  
    },
    {
      'questionText': 'Grant Gustin plays which superhero on the CW show of the same name?',
      'answers':[{'text': 'The Flash','score':1},
      {'text': 'The Arrow','score':0},
      {'text': 'Daredevil','score':0},
      {'text': 'Black Canary','score':0}
    ],  
    },
    {
      'questionText': 'In the 1993 Disney animated series, what is the name of Bonker\'s second partner?',
      'answers':[{'text': 'Dick Tracy','score':0},
      {'text': 'Eddie Valiant','score':0},
      {'text': 'Miranda Wright','score':0},
      {'text': 'Dr. Ludwig von Drake','score':0}
    ],  
    },

  ];
  
var _questionIndex=0;
var _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }
  void _answerQuestion(int score)
  {
        _totalScore += score;

    setState(() {
      _questionIndex+=1;
    });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return  Scaffold(
      appBar:  AppBar(
        title:  Text('General Quiz Page'),
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
      ),
      body:  _questionIndex < _questions.length
       ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),

        
    );
  }
}
