
import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
import 'package:myapp/models/user.dart';


import '../utlis/database_helper.dart';



//var now = new DateTime.now();

class Register extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new MYRegisterPage();
      }
    
    }
    
    class MYRegisterPage extends State<Register> {

final  TextEditingController _firstname =new TextEditingController();
          final TextEditingController _lastname =  new TextEditingController();
final  TextEditingController _email =new TextEditingController();
          final TextEditingController _password =  new TextEditingController();
                    final TextEditingController _answer =  new TextEditingController();
                    //TextEditingController _index =new TextEditingController();
                    
//final DateTime _startdate =new DateTime.now();
    int _selectedQuestion = 0;

      List<DropdownMenuItem<int>> questionList = [];

  void getQuestion()
  {
    questionList = [];
    questionList.add(new DropdownMenuItem(
      child: new Text('Select Question'),
      value: 0,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('Where is your birth place?'),
      value: 1,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('What is your favourite food?'),
      value: 2,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('What is your Nick name?'),
      value: 3,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('What is your Pet name?'),
      value: 4,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('What is your favourite game?'),
      value: 5,
    ));
  }

      
                    

  final dbHelper = new DatabaseHelper();


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

        getQuestion();

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Registeration Page'),
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
      ),
      body: new Container(
        alignment: Alignment.center,
        child: new ListView(
          padding: const EdgeInsets.all(2.0),
          children: <Widget>[
            Center(
                        child: new Container(
                          margin: EdgeInsets.only(top: 20.0),
                          child: new Text(
                            'Register',
                            style: TextStyle(
                                fontSize: 25.0, ),
                          ),
                        ),
                      ),
                      new ListTile(
                        
                        leading: const Icon(Icons.person),
                        title: new TextFormField(
                          controller: _firstname,
                          decoration: new InputDecoration(
                            hintText: 'Please enter first Name',
                            labelText: 'Enter Your first Name',
                          ),
                          keyboardType: TextInputType.text,
                        ),
                      ),
                      new ListTile(
                        leading: const Icon(Icons.person),
                        title: new TextFormField(
                          controller: _lastname,
                          decoration: new InputDecoration(
                            hintText: 'Please enter Last Name',
                            labelText: 'Enter Your Last Name',
                          ),
                          keyboardType: TextInputType.text,
                        ),
                      ),
                      new ListTile(
                        leading: const Icon(Icons.email),
                        title: new TextFormField(
                          controller: _email,
                          decoration: new InputDecoration(
                            hintText: 'Please enter Email-id',
                            labelText: 'Enter Your Email-id',
                          ),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      new ListTile(
                        leading: const Icon(Icons.lock),
                        title: new TextFormField(
                          controller: _password,
                          decoration: new InputDecoration(
                            hintText: 'Choose a password',
                            labelText: 'Enter Your Password',
                          ),
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: true,
                        ),
                      ),
                      new ListTile(

                        leading: const Icon(Icons.arrow_downward),
                        title: new DropdownButton(
                          
                          hint: new Text('Select Question'),
                          items: questionList,
                           value: _selectedQuestion,
                            onChanged: (value) {
                            setState(() {
                                  _selectedQuestion = value;
                                  
                              });
                              },
                       isExpanded: true,
                         ),
                        
                      ),
                      new ListTile(
                        leading: const Icon(Icons.question_answer),
                        title: new TextFormField(
                          controller: _answer,
                          decoration: new InputDecoration(
                            hintText: 'Enter answer',
                            labelText: 'Enter Your answer',
                          ),
                          keyboardType: TextInputType.text,
                          obscureText: true,
                        ),
                      ),
                    Row(children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                         onPressed: ()=> {_insert()},
        //                 onPressed: () async {
        //   Client rnd = testClients[math.Random().nextInt(testClients.length)];
        //   await DBProvider.db.newClient(rnd);
        //   setState(() {});
        // },
            color: Colors.pinkAccent,
            child: new Text('Register'),
            textColor: Colors.white,
                      ),
                    ),
                  ])

                      
                    ],
          
        )
      ),  
    );
  }
void _insert() async {
    // row to insert
    // Map<String, dynamic> row = {
    //   DatabaseHelper.columnName : _firstname.text+_lastname.text,
    //   DatabaseHelper.columnEmail : _email.text,
    //  DatabaseHelper.columnPassword : _password.text,
    //  DatabaseHelper.columnQuestion: _selectedQuestion.toString(),
    //  DatabaseHelper.columnAnswer: _answer.text,
    //  DatabaseHelper.columnStartdate:new DateFormat("dd-MM-yyyy").format(now),
    //  DatabaseHelper.columnEnddate: Null,
    //  DatabaseHelper.columnStatus: 'A', 
    //     };
    var finalname= _firstname.text.trim()+" "+_lastname.text.trim();
    final id = await dbHelper.insert(new User(finalname,
     _password.text.trim(), _email.text.trim(),_answer.text.trim(), _selectedQuestion.toString(),  'A'));
  
    // final id = await dbHelper.insert(new User(0,_firstname.text.trim()+" "+_lastname.text.trim(),
    //  _password.text.trim()));
    _showDialog(_email.text.trim());
    print('inserted row id: $id');

  }
  void _showDialog(String email) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert!"),
          content: new Text("$email is successfuly registered."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                
                     _firstname.clear();
                     _lastname.clear();
                     _email.clear();
                     _answer.clear();
                     _password.clear();
                                     Navigator.of(context).pop();


              },
            ),
          ],
        );
      },
    );
  }
}

 // String name=_firstname.text+_lastname.text; 
//         String email=_email.text;
//     String password=_password.text;
//     String question=_selectedQuestion.toString();
//     String answer=_answer.text;