
import 'package:flutter/material.dart';

import '../utlis/database_helper.dart';
import './ResetPassword.dart';


//var now = new DateTime.now();

class ForgetPassword extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new MYForgetPasswordPage();
      }
    
    }
    
    class MYForgetPasswordPage extends State<ForgetPassword> {

final  TextEditingController _email =new TextEditingController();
         // final TextEditingController _password =  new TextEditingController();
                    final TextEditingController _answer =  new TextEditingController();
                    //TextEditingController _index =new TextEditingController();
                    
//final DateTime _startdate =new DateTime.now();
    int _selectedQuestion = 0;

      List<DropdownMenuItem<int>> questionList = [];

  void getQuestion()
  {
    questionList = [];
    questionList.add(new DropdownMenuItem(
      child: new Text('Select Question'),
      value: 0,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('Where is your birth place?'),
      value: 1,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('What is your favourite food?'),
      value: 2,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('What is your Nick name?'),
      value: 3,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('What is your Pet name?'),
      value: 4,
    ));
    questionList.add(new DropdownMenuItem(
      child: new Text('What is your favourite game?'),
      value: 5,
    ));
  }

      
                    

  final dbHelper = new DatabaseHelper();


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

        getQuestion();

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Password Recovery Page'),
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
      ),
      body: new Container(
        alignment: Alignment.center,
        child: new ListView(
          padding: const EdgeInsets.all(2.0),
          children: <Widget>[
            Center(
                        child: new Container(
                          margin: EdgeInsets.only(top: 20.0),
                          child: new Text(
                            'Recover Password',
                            style: TextStyle(
                                fontSize: 25.0, ),
                          ),
                        ),
                      ),
                      
                      
                      new ListTile(
                        leading: const Icon(Icons.email),
                        title: new TextFormField(
                          controller: _email,
                          decoration: new InputDecoration(
                            hintText: 'Please enter Email-id',
                            labelText: 'Enter Your Email-id',
                          ),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      
                      new ListTile(

                        leading: const Icon(Icons.arrow_downward),
                        title: new DropdownButton(
                          
                          hint: new Text('Select Question'),
                          items: questionList,
                           value: _selectedQuestion,
                            onChanged: (value) {
                            setState(() {
                                  _selectedQuestion = value;
                                  
                              });
                              },
                       isExpanded: true,
                         ),
                        
                      ),
                      new ListTile(
                        leading: const Icon(Icons.question_answer),
                        title: new TextFormField(
                          controller: _answer,
                          decoration: new InputDecoration(
                            hintText: 'Enter answer',
                            labelText: 'Enter Your answer',
                          ),
                          keyboardType: TextInputType.text,
                          obscureText: true,
                        ),
                      ),
                    Row(children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                         onPressed: ()=> {_verify()},
        //                 onPressed: () async {
        //   Client rnd = testClients[math.Random().nextInt(testClients.length)];
        //   await DBProvider.db.newClient(rnd);
        //   setState(() {});
        // },
            color: Colors.pinkAccent,
            child: new Text('Verify User'),
            textColor: Colors.white,
                      ),
                    ),
                  ])

                      
                    ],
          
        )
      ),  
    );
  }
void _verify() async {
   
   // final id = await dbHelper.insert(new User());
  final result = await dbHelper.verify(_email.text.trim(), _selectedQuestion.toString(),_answer.text.trim());
        
        if (result=='TRUE'){
                  _showDialogTrue(_email.text.trim());

        }
        else{
                  _showDialogFalse(_email.text.trim());

        }
      
    print('inserted row id: $result');

  }
  void _showDialogTrue(String email) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert!"),
          content: new Text("$email is verified successfully."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ResetPassword(email)),
     );
  }, 
            ),
          ],
        );
      },
    );
  }
  void _showDialogFalse(String email) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert!"),
          content: new Text("$email is not a registered User."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

 // String name=_firstname.text+_lastname.text; 
//         String email=_email.text;
//     String password=_password.text;
//     String question=_selectedQuestion.toString();
//     String answer=_answer.text;