
//import 'dart:html';

import 'package:flutter/material.dart';

import '../utlis/database_helper.dart';
import './home.dart';



//var now = new DateTime.now();

class ResetPassword extends StatefulWidget{
   final String _email;

   ResetPassword(this._email) ;


  @override
  State<StatefulWidget> createState() {
    return new MYResetPasswordPage(_email);
      }
    
    }
    
    class MYResetPasswordPage extends State<ResetPassword> {
//final  TextEditingController _email =new TextEditingController();
          final TextEditingController _password =  new TextEditingController();
          final TextEditingController _confirmpassword= new TextEditingController();
                    //final TextEditingController _answer =  new TextEditingController();
                    
    

      
                    

  final dbHelper = new DatabaseHelper();
   final String _email;

  MYResetPasswordPage(this._email);
  //String _email=;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build


    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Password Recovery Page'),
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
      ),
      body: new Container(
        alignment: Alignment.center,
        child: new ListView(
          padding: const EdgeInsets.all(2.0),
          children: <Widget>[
            Center(
                        child: new Container(
                          margin: EdgeInsets.only(top: 20.0),
                          child: new Text(
                            'Recover Password',
                            style: TextStyle(
                                fontSize: 25.0, ),
                          ),
                        ),
                      ),
                      
                       new ListTile(
                        leading: const Icon(Icons.lock),
                        title: new TextFormField(
                          controller: _password,
                          
                          decoration: new InputDecoration(
                            hintText: 'Choose a password',
                            labelText: 'Enter Your New Password',
                          ),
                          validator: (value){
                             if (value.isEmpty) {
                                  return 'Please enter Password';
                             }
                             return null;
                          },
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: true,
                        ),
                      ),
                       new ListTile(
                        leading: const Icon(Icons.lock),
                        title: new TextFormField(
                          
                          controller: _confirmpassword,
                          decoration: new InputDecoration(
                           // hintText: 'Choose a password',
                            labelText: 'Retype Your Password',
                          ),
                          validator: (value){
                             if (value != _password.text) {
                                  return 'Password is not matching';
                             }
                             return null;
                          },
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: true,
                        ),
                      ),
                      
                      
                      
                    Row(children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                         onPressed: ()=> {_update()},
       
            color: Colors.pinkAccent,
            child: new Text('Update Password'),
            textColor: Colors.white,
                      ),
                    ),
                  ])

                      
                    ],
          
        )
      ),  
    );
  }
void _update() async {
   
   // final id = await dbHelper.insert(new User());
  final result = await dbHelper.update(_email,_confirmpassword.text.trim());
        
         if (result=='TRUE'){
                 _showDialogTrue(_email);

        }
        else{
                  //_showDialogFalse(_email));

        }
      
    print('inserted row id: $result');

  }
  void _showDialogTrue(String email) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert!"),
          content: new Text("$email's Password changed successfully."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MyApp()),
     );
  },
            ),
          ],
        );
      },
    );
  }
  
}

 