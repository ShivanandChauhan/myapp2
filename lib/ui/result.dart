import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetHandler;

  Result(this.resultScore, this.resetHandler);

  String get resultPhrase {
    //String resultText;
    // if (resultScore <= 3) {
    //   resultText = 'You are awesome and innocent!';
    // } else if (resultScore <= 12) {
    //   resultText = 'Pretty likeable!';
    // } else if (resultScore <= 16) {
    //   resultText = 'You are ... strange?!';
    // } else {
    //   resultText = 'You are so bad!';
    // }

    return 'You have given correct answer of '+resultScore.toString()+' questions.';
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            resultPhrase,
            style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            child: Text(
              'Restart Quiz!',
            ),
            textColor: Colors.pinkAccent,
            onPressed: resetHandler,
          ),
        ],
      ),
    );
  }
}
