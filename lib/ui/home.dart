import 'package:flutter/material.dart';
import './Register.dart';
import './ForgetPassword.dart';
import '../utlis/database_helper.dart';
import './LoginAction.dart';



class MyApp extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new MYhomepage();
      }
    
    }
    
    class MYhomepage extends State<MyApp> {
        final dbHelper = new DatabaseHelper();
final  TextEditingController _email =new TextEditingController();
          final TextEditingController _password =  new TextEditingController();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('LOGIN PAGE'),
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
      ),
      body: new Container(
        alignment: Alignment.center,
        child: new ListView(
          padding: const EdgeInsets.all(2.0),
          children: <Widget>[
            Center(
                        child: new Container(
                          margin: EdgeInsets.only(top: 20.0),
                          child: new Text(
                            'Login',
                            style: TextStyle(
                                fontSize: 25.0, ),
                          ),
                        ),
                      ),
                      new ListTile(
                        leading: const Icon(Icons.person),
                        title: new TextFormField(
                          controller: _email,
                          decoration: new InputDecoration(
                            hintText: 'Please enter email',
                            labelText: 'Enter Your userID',
                          ),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      new ListTile(
                        leading: const Icon(Icons.lock),
                        title: new TextFormField(
                          controller: _password,
                          decoration: new InputDecoration(
                            hintText: 'Please enter password',
                            labelText: 'Enter Your Password',
                          ),
                          keyboardType: TextInputType.emailAddress,
                          obscureText: true,
                        ),
                      ),
                      
                      Row(children: <Widget>[
                          Expanded(
                      child: RaisedButton(
                         onPressed: () {_loginfun();},
                       
            color: Colors.pinkAccent,
            child: new Text('Loggin'),
            textColor: Colors.white,
                      ),
                    ),

                    
                       ]),
                       Row(children: <Widget>[
                          Expanded(
                      child: RaisedButton(
                       onPressed: ()
                       {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ForgetPassword()),
     );
  }, 
            color: Colors.pinkAccent,
            child: new Text('Forget Password'),
            textColor: Colors.white,
                      ),
                    ),

                    Expanded(
                      child: RaisedButton(
                         onPressed: ()
                         {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Register()),
     );
  }, 
            color: Colors.pinkAccent,
            child: new Text('Register'),
            textColor: Colors.white,
                      ),
                    ),
                       ])
                    ],
          
        )
      ),  
    );
  }
 void _loginfun() async {
   
   // final id = await dbHelper.insert(new User());
  final result = await dbHelper.loginaction(_email.text.trim(),_password.text.trim());
        
         if (result=='TRUE'){
                 _showDialogTrue(_email.text.trim());

        }
        else{
                  _showDialogFalse(_email.text.trim());

        }
      
    //print('inserted row id: $result');

  }
   void _showDialogTrue(String email) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert!"),
          content: new Text("$email is  successfully Logged In."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginAction()),
     );
  }, 
            ),
          ],
        );
      },
    );
  }
  void _showDialogFalse(String email) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert!"),
          content: new Text("Invalid Email-ID or Password."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                _password.clear();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}