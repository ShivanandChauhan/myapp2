//import 'dart:html';

import 'package:flutter/material.dart';
import './ITQuiz.dart';
import './GeneralQuiz.dart';

class LoginAction extends StatefulWidget {
 
  @override
  State<StatefulWidget> createState() {
    return new MYLoginActionPage();
  }
}

class MYLoginActionPage extends State<LoginAction> {
 
  

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return  Scaffold(
      appBar:  AppBar(
        title:  Text('Quiz Page'),
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
      ),
      body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
          //crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
       
                          SizedBox(
                            width: 100,
                            
                      child: RaisedButton(
                         
                         onPressed: () {
            Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ITQuiz()));
                         },
                       
            color: Colors.pinkAccent,
            child: new Text('IT QUIZ'),
            textColor: Colors.white,
                      ),
                          ),
                    

                    RaisedButton(
                         onPressed: () {
            Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GeneralQuiz()));
                         },
                       
            color: Colors.pinkAccent,
            child: new Text('GENERAL QUIZ'),
            textColor: Colors.white,
                      ),
                    
          
       
                       ]),
      ),
      

        
    );
  }
}
