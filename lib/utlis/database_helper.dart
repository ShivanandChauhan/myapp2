import 'dart:io' show Directory;
import 'package:myapp/models/user.dart';
import 'package:path/path.dart' show join;

import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart' show getApplicationDocumentsDirectory;

class DatabaseHelper  {

  

  static final table = "New_User_Master_Table";



  static final columnId = "Seqid";
  static final columnName = "UserName";
   static final columnEmail = 'Email';
   static final columnQuestion = 'Question';
    static final columnPassword = "Password";
  static final columnAnswer = 'Answer';
  static final columnStartdate = 'Startdate';
  static final columnStatus = 'Status';
  static final columnEnddate = 'Enddate';


static final DatabaseHelper _instance =new DatabaseHelper.internal();

factory  DatabaseHelper() =>_instance;
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    // lazily instantiate the db the first time it is accessed
    _db = await _initDatabase();
    return _db;
  }
  DatabaseHelper.internal();

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "New_User_master_db.db");
    
   var ourDb=  await openDatabase(path,version: 1,onCreate: _onCreate);
   return ourDb;
  }
    
     void _onCreate(Database db, int newVersion) async {
            await db.execute("Create table $table ($columnId INTEGER ,$columnName  Text,$columnEmail Text PRIMARY KEY NOT NULL, $columnPassword  Text NOT NULL,$columnQuestion  Text,$columnAnswer Text,$columnStartdate INTEGER DEFAULT (strftime('%s','now')),$columnEnddate   DATE DEFAULT NULL,$columnStatus Text)");           

  }
  
  
  insert(User user) async {
    var dbclient =await db;
 var newtable = await dbclient.rawQuery("SELECT MAX(Seqid)+1 as id FROM $table");
    int id = newtable.first["Seqid"];   
     var res = await dbclient.rawInsert(
      "INSERT Into $table (Seqid,UserName,Email,Password,Question,Answer,Status) VALUES (?,?,?,?,?,?,?)",
      [id,user.name,user.email,user.password,user.qusetion,user.answer,user.status]);
    return res;
  }

  verify(String email,String question, String answer) async{
        var clientvrfy =await db;
        var fres='';
            var reslt = await clientvrfy.rawQuery("SELECT * FROM $table WHERE Email='$email' and Question='$question' and Answer='$answer'");
           
            if(reslt.isNotEmpty)
            { fres='TRUE'; }
            else
            { fres='FALSE'; }
     return fres;

  }
  loginaction(String email,String password) async{
        var clientvrfy =await db;
        var fres='';
           var reslt;
        try{

        
             reslt = await clientvrfy.rawQuery("SELECT * FROM $table WHERE Email='$email' and Password='$password' and Status='A' and Enddate is null");
        } 
        catch(e){
          print("Exception in Update = $e");
        }   
          
            if(reslt.isNotEmpty)
            { fres='TRUE'; }
            else
            { fres='FALSE'; }
     return fres;

  }
  update(String email,String changedPassword) async {
         var clientvrfy =await db;
        var fres='';
        var reslt;
        try{

        
             reslt = await clientvrfy.rawUpdate("Update $table  set Password='$changedPassword'  WHERE Email='$email'");
        } 
        catch(e){
          print("Exception in Update = $e");
        }   
          
         if(reslt > 0)
            { fres='TRUE'; }
            else
            { fres='FALSE'; }
     return fres;

  }
}